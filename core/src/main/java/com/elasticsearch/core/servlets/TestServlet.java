package com.elasticsearch.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = Servlet.class, property = { "sling.servlet.paths=/bin/testservlet",
		"sling.servlet.name=TEST SERVLET", "sling.servlet.methods={GET/POST}" })
public class TestServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	protected void doGet(SlingHttpServletRequest req, SlingHttpServletResponse res)
			throws ServletException, IOException {

		/* This Servlet is just for testing Purpose only */

		PrintWriter writer = res.getWriter();

		String path = "/content/dam/test-images/abc.jpg";

		Resource resource = req.getResourceResolver().getResource(path);
		ValueMap valueMap = resource.getValueMap();
		Set<String> keySet = valueMap.keySet();
		for (String key : keySet) {
			writer.println("\nKey: " + key + "\tValue: " + valueMap.get(key));

		}
	}
}
