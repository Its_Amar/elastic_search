package com.elasticsearch.core.service;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/*
 * This Annotation will act as a Configuration in Configuration Manager
 * 
 * @author Sandeep.Leburu
 * 
 * */

@ObjectClassDefinition(name = "Elastic Index Configuration")
public @interface ElasticSearchConfig {
	
	@AttributeDefinition(name = "Cluster Name", description = "Elastic Search Cluster Name")
	String getESClusterName() default "elaticsearch";

	@AttributeDefinition(name = "ES Host IP", description = "ES Host Ip")
	String getESHost() default "localhost";

	@AttributeDefinition(name = "ES Port Number", description = "ES Port number")
	int getESPort() default 9200;
	
	@AttributeDefinition(name = "Index Fields", description = "Give the Index Fields, You want to index into Elastic Search")
	String[] getEsFields() default {"jcr:created", "jcr:uuid", "dc:format", "tiff:ImageLength", "tiff:ImageWidth", "dam:size"};
	
	@AttributeDefinition(name = "Use Elastic Search", description = "Check for Elastic Search")
	boolean getESchoice() default false;
	
}
