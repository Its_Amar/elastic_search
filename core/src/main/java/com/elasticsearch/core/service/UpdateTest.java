package com.elasticsearch.core.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = Servlet.class, name = "UpdateTest", property = { "sling.servlet.paths=/bin/updatetest",
		"sling.servlet.methods=GET/POST" })
public class UpdateTest extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	@Reference
	ResourceResolverFactory resolverFactory;

//	@Reference
//	ConfigurationService cs;

	private ResourceResolver resourceResolver;
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	protected void doGet(SlingHttpServletRequest req, SlingHttpServletResponse res)
			throws ServletException, IOException {
		String value = null;
		// initialize resource resolver
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(ResourceResolverFactory.SUBSERVICE, "testArchivaluser");

		try {
			resourceResolver = resolverFactory.getServiceResourceResolver(param);
		} catch (LoginException e1) {
			log.error("Unable to fetch resource resolver");
			res.sendError(400);
			return;
		}

		if (resourceResolver == null) {
			log.error("Null resource resolver check access to content/dam");
			res.sendError(400);
			return;
		}

//		RestHighLevelClient client = cs.getRestHighLevelClient();
//		res.getWriter().write(client.toString());
//		try {
//			Resource resource = resourceResolver.getResource("/content/dam/example/Work quotes.jpg/jcr:content");
//			ValueMap values = resource.getValueMap();
//			Set<String> keySet = values.keySet();
//			for (String key : keySet) {
//				if (key.equalsIgnoreCase("jcr:title")) {
//					value = String.valueOf(values.get(key));
//					log.info(value);
//				}
//			}
//			UpdateRequest updateRequest = new UpdateRequest("twitter", "1");
//			// .script(new Script("ctx._source.title = value"));
//			Map<String, Object> parameters = new HashMap<>();
//			parameters.put("message", "testmessage");
//			Script inline = new Script(ScriptType.INLINE, "painless", "ctx._source.message = params.message",
//					parameters);
//			updateRequest.script(inline);
//			UpdateResponse response = client.update(updateRequest, RequestOptions.DEFAULT);
//			response.getId();
//		} catch (ElasticsearchException exception) {
//			if (exception.status() == RestStatus.NOT_FOUND) {
//				log.error("Not Found");
//			}
//		}
//
//	}
//
//	ActionListener<UpdateResponse> listener = new ActionListener<UpdateResponse>() {
//
//		@Override
//		public void onFailure(Exception e) {
//			log.error(e.getMessage());
//		}
//
//		@Override
//		public void onResponse(UpdateResponse response) {
//			String abc = response.toString();
//			log.info(abc);
//
//		}
	};
}
