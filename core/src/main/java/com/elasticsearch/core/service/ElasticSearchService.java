package com.elasticsearch.core.service;

import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.index.reindex.BulkByScrollResponse;

public interface ElasticSearchService {

//	RestHighLevelClient getRestHighLevelClient();

//	List<String> getElasticSearchIndexFields();

	IndexResponse indexAsset(String path);

	UpdateResponse updateAssetIndex(String path);

	BulkByScrollResponse deleteAssetIndex(String path);

	Map<String, List<Resource>> searchIndex(ResourceResolver resolver, Map<String, String[]> map, long limit,
			long offset);

	boolean getESChoice();

}
