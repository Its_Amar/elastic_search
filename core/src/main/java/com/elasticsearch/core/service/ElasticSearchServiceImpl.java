package com.elasticsearch.core.service;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpHost;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchPhrasePrefixQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;

@Component(service = ElasticSearchService.class, name = "Elastic Search Service")
@Designate(ocd = ElasticSearchConfig.class)
public class ElasticSearchServiceImpl implements ElasticSearchService {

	Logger loger = LoggerFactory.getLogger(this.getClass());

	private ElasticSearchConfig config;

	@Reference
	ResourceResolverFactory factory;

	@Activate
	private void activeConfig(ElasticSearchConfig config) {
		this.config = config;
	}

	@Override
	public IndexResponse indexAsset(String path) {
		// Invoke configured ResourceResolver
		ResourceResolver resolver = getConfiguredResourceResolver();

		// Invoke RestHightLevelClient from Configuration
		RestHighLevelClient client = getRestHighLevelClient();

		// Invoke Index fields from Configuration
		List<String> fields = getElasticSearchIndexFields();
		loger.info("Index Fieds from Configuration: " + fields.toString());

		Resource resource = resolver.getResource(path);

		// Create Map to set index data
		Map<String, Object> data = new HashMap<String, Object>();

		String absPath = resource.getPath();
		String name = resource.getName();

		data.put("title", name);
		data.put("absolutepath", absPath);

		ValueMap valueMap = resource.getValueMap();
		Set<String> keySet = valueMap.keySet();

		for (String key : keySet) {
			if (key.equalsIgnoreCase("jcr:created")) {
				Date date = valueMap.get(key, Date.class);
				SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
				String finaldate = formatdate.format(date);
				data.put(key, finaldate);// createdDate
			} else if (fields.contains(key)) {
				data.put(key, valueMap.get(key));
			}
		}

		Asset asset = resource.adaptTo(Asset.class);
		Map<String, Object> metadata = asset.getMetadata();
		Set<String> set = metadata.keySet();
		loger.info("Asset API Called");

		for (String key : set) {
			if (fields.contains(key)) {
				data.put(key, metadata.get(key));
			}
		}

		// Create Index Request & pass index data as source
		IndexRequest indexRequest = new IndexRequest("dam_asset").source(data);

		IndexResponse response = null;
		try {
			response = client.index(indexRequest, RequestOptions.DEFAULT);
		} catch (IOException e) {
			loger.debug(e.toString());
		}

		if (response != null) {
			String ind = response.getIndex();
			String id = response.getId();
			loger.info("\nIndex: " + ind + "\nId: " + id);

			// Adding both index & id as properties to 'metadata'
			addIndexToMetadata(path, ind, id);
		}
		return response;
	}

	@Override
	public UpdateResponse updateAssetIndex(String path) {
		
		RestHighLevelClient client = getRestHighLevelClient();
		ResourceResolver resolver = getConfiguredResourceResolver();
		String absolutepath = null;
		
		// Getting 'absolutepath'
		if (path.contains("jcr:content")) {
			String search = "/jcr:content";
	        int index = path.lastIndexOf(search);
	        if (index > 0) {
	        	absolutepath = path.substring(0, index);
	        }
		}
		// Get the Resource of 'metadata' to get 'elasticId' and 'elasticIndex'
		Resource resource = resolver.getResource(absolutepath.concat("/jcr:content/metadata"));
		
		// Get the Resource w.r.t update
		Resource resourceChange = resolver.getResource(path);
		
		ValueMap valueMap = resource.getValueMap();
		Set<String> keySet = valueMap.keySet();
		String elasticIndex = null, elasticId =null;
		for(String key:keySet){
			if(key.equals("elasticId")){
				elasticId = valueMap.get(key).toString();
			}
			else if(key.equals("elasticIndex")){
				elasticIndex = valueMap.get(key).toString();
			}
		}
		
		//To get the indexed fields from the updated path
		ValueMap valueMapFields = resourceChange.getValueMap();
		Set<String> keySetFields = valueMapFields.keySet();
		List<String> fields = getElasticSearchIndexFields();
		
		UpdateRequest updateRequest = new UpdateRequest();
		updateRequest.index(elasticIndex);
		//updateRequest.type("_doc");
		updateRequest.id(elasticId);
		UpdateResponse updateResponse = null;
		
		for(String key:keySetFields){
			for(String field: fields){
    			if (key.equals(field)) {
					loger.info(key + " : " + valueMapFields.get(key));
					try {
						updateRequest.doc(jsonBuilder().startObject().field(key,valueMapFields.get(key)).endObject());
						updateResponse = client.update(updateRequest, RequestOptions.DEFAULT);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
    			}
			
		}
	
	return updateResponse;
		
		
		/*// Not working
		RestHighLevelClient client = getRestHighLevelClient();
		ResourceResolver resourceResolver = getConfiguredResourceResolver();
		
		loger.info(path);
		try {
			//Resource resource = resourceResolver.getResource(path);
			//String finalPath = resource.getParent().getPath();
			// String name=resource.getName();
			String finalPath = null;
			if (path.contains("jcr:content")) {
			String search = "/jcr:content";
			int index = path.lastIndexOf(search);
			if (index > 0) {
        	finalPath = path.substring(0, index);
			}
			}
			loger.info("Final path:" +finalPath);
			Resource resource = resourceResolver.getResource(finalPath.concat("/jcr:content/metadata"));
			ValueMap values = resource.getValueMap();
			Set<String> keySet = values.keySet();
			String value = null;
			for (String key : keySet) {
				if (key.equalsIgnoreCase("elasticId")) {
					value = String.valueOf(values.get(key));
					loger.info("elasticId" +value);

				}
			}
			UpdateByQueryRequest request = new UpdateByQueryRequest("dam_asset");
			request.setConflicts("proceed");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("absolutepath", finalPath);
			loger.info(parameters.get("absolutepath").toString());
			Script inline = new Script(ScriptType.INLINE, "painless", "ctx._source.absolutepath =params.absolutepath",
					parameters);
			request.setQuery(new TermQueryBuilder("absolutepath.keyword", finalPath)).setScript(inline);
			BulkByScrollResponse bulkResponse = client.updateByQuery(request, RequestOptions.DEFAULT);
			List<SearchFailure> failures = bulkResponse.getSearchFailures();
			Iterator<SearchFailure> failitr = failures.iterator();
			while (failitr.hasNext()) {
				SearchFailure eachFail = failitr.next();
				String index = eachFail.getIndex();
				loger.info(index);
			}

		} catch (ElasticsearchException | IOException exception) {

		}
		return null;*/
		
	}

	@Override
	public BulkByScrollResponse deleteAssetIndex(String path) {
		// Invoke RestHightLevelClient from Configuration
		RestHighLevelClient client = getRestHighLevelClient();

		// Create DeleteByQueryRequest & set query to match 'absolutepath'
		DeleteByQueryRequest deleterequest = new DeleteByQueryRequest("dam_asset");
		deleterequest.setConflicts("proceed");
		deleterequest.setQuery(new TermQueryBuilder("absolutepath.keyword", path));

		BulkByScrollResponse bulkresponse = null;
		try {
			// Invoking BulkByScrollResponse by using RestHighLevelClient &
			// DeleteByQueryRequest
			bulkresponse = client.deleteByQuery(deleterequest, RequestOptions.DEFAULT);

			loger.info("\nBulk Response: " + bulkresponse.toString());

		} catch (IOException e) {
			loger.debug(e.toString());
		}
		return bulkresponse;
	}

	private RestHighLevelClient getRestHighLevelClient() {
		RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(config.getESHost(), config.getESPort())));
		loger.info(client.toString());
		return client;
	}

	private ResourceResolver getConfiguredResourceResolver() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ResourceResolverFactory.SUBSERVICE, "ELASTICKIBANA");

		ResourceResolver resolver = null;
		try {
			resolver = factory.getServiceResourceResolver(map);
		} catch (LoginException e) {
			loger.debug(e.toString());
		}
		return resolver;
	}

	private List<String> getElasticSearchIndexFields() {
		List<String> fields = Arrays.asList(config.getEsFields());
		return fields;
	}

	private void addIndexToMetadata(String parentPath, String elasticIndex, String elasticId) {

		// Get Configured ResourceResolver
		ResourceResolver resolver = getConfiguredResourceResolver();

		// Get the Resource of 'metadata'
		Resource resource = resolver.getResource(parentPath.concat("/jcr:content/metadata"));

		try {
			// Create ModifiableValueMap to add index & id as properties
			ModifiableValueMap valueMap = resource.adaptTo(ModifiableValueMap.class);
			valueMap.put("elasticIndex", elasticIndex);
			valueMap.put("elasticId", elasticId);
			resource.getResourceResolver().commit();

		} catch (PersistenceException e) {
			loger.debug(e.toString());
		}
	}

	@Override
	public boolean getESChoice() {
		// TODO Auto-generated method stub
		boolean myChoice = config.getESchoice();
		return myChoice;
	}

	@Override
	public Map<String, List<Resource>> searchIndex(ResourceResolver resolver, Map<String, String[]> predicateMap,
			long limit, long offset) {
		Map<String, List<Resource>> resultsMap = new HashMap<String, List<Resource>>();
		List<Resource> results = new ArrayList<Resource>();
		for (String key : predicateMap.keySet()) {
			loger.info(key + " : " + Arrays.toString(predicateMap.get(key)));
		}
		SearchRequest searchRequest = new SearchRequest();
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		String queryString = predicateMap.get("fulltext")[0];
		loger.info("Query String : " + queryString);
		
		List<String> fields = Arrays.asList(config.getEsFields());
		loger.info("Index Fieds from Configuration: " + fields.toString());
		
		MatchPhrasePrefixQueryBuilder matchPhrase1 = QueryBuilders.matchPhrasePrefixQuery("title", queryString);
		MatchPhrasePrefixQueryBuilder matchPhrase2 = QueryBuilders.matchPhrasePrefixQuery("absolutePath", queryString);
		
		BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery().should(matchPhrase1).should(matchPhrase2);

		for(String key: fields){
			if(!(key.equalsIgnoreCase("jcr:created") || key.equalsIgnoreCase("tiff:ImageLength") || key.equalsIgnoreCase("tiff:ImageWidth") || key.equalsIgnoreCase("dam:size"))){
			MatchPhrasePrefixQueryBuilder matchPhrase = QueryBuilders.matchPhrasePrefixQuery(key, queryString);
			queryBuilder = queryBuilder.should(matchPhrase);
			}
			}		
		searchSourceBuilder.query(queryBuilder);
		searchRequest.source(searchSourceBuilder);
		
		try {
			SearchResponse response = getRestHighLevelClient().search(searchRequest, RequestOptions.DEFAULT);

			loger.info("No of hits " + response.getHits().getTotalHits().value);
			SearchHit[] searchHits = response.getHits().getHits();
	
		for (SearchHit hit : searchHits) {
				Map<String, Object> sourceAsMap = hit.getSourceAsMap();
//				for (String key : sourceAsMap.keySet()) {
//					loger.info(key + " : " + sourceAsMap.get(key));
//				}
				Resource res = resolver.resolve(sourceAsMap.get("absolutepath").toString());
				if (res != null) {
					results.add(res);
					loger.info("RES :"+res);
				}
			}
			loger.info("results before addition: "+results);
			resultsMap.put(predicateMap.get("location")[0], results);
			loger.info("resultsMap: " + resultsMap);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			loger.error("Error", e);
		}

		return resultsMap;
	}

}
