package com.elasticsearch.core.listeners;

import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.elasticsearch.action.index.IndexResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.elasticsearch.core.service.ElasticSearchService;

/*
 * This Class activates the EvenTListener with respect to Add/Upload Event on given EventPath.
 * 
 * @author Sandeep.Leburu
 * 
 * */

@Component(service = ResourceChangeListener.class, property = { ResourceChangeListener.PATHS + "=" + "/content/dam/",
		ResourceChangeListener.CHANGES + "=" + "ADDED" }, name = "Add Event Listener")
public class UploadAssetEventListener implements ResourceChangeListener {

	private Logger loger = LoggerFactory.getLogger(this.getClass());

	@Reference
	ElasticSearchService els;

	@Override
	public void onChange(List<ResourceChange> changes) {

		Iterator<ResourceChange> listIterator = changes.iterator();
		while (listIterator.hasNext()) {
			ResourceChange resourceChange = (ResourceChange) listIterator.next();
			String path = resourceChange.getPath();
			if (!(path.contains("jcr:content"))) {

				loger.info("Final Filtered Path: " + path);

				try {
					// Event Upload may get Delayed for Some times, So adding Thread.sleep()
					Thread.sleep(4000);

					IndexResponse indexResponse = els.indexAsset(path);

					loger.info("Index Response: " + indexResponse.toString());
				} catch (InterruptedException e) {
					loger.debug(e.toString());
				}
			}
		}
	}
}