package com.elasticsearch.core.listeners;

import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.elasticsearch.action.update.UpdateResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.elasticsearch.core.service.ElasticSearchService;

//TBD

@Component(service = ResourceChangeListener.class, property = { ResourceChangeListener.PATHS + "=" + "/content/dam/",
		ResourceChangeListener.CHANGES + "=" + "CHANGED" },name = "Update Event Listener")
public class UpdateAssetEventListener implements ResourceChangeListener {

	private Logger loger = LoggerFactory.getLogger(this.getClass());

	@Reference
	ResourceResolverFactory resolverFactory;

	@Reference
	ElasticSearchService els;
	
	@Override
	public void onChange(List<ResourceChange> changes) {

		Iterator<ResourceChange> listIterator = changes.iterator();
		
		while (listIterator.hasNext()) {
			ResourceChange resourceChange = (ResourceChange) listIterator.next();
			String path = resourceChange.getPath();
			loger.info("Path of the update : " + path);
				
			UpdateResponse updateResponse = els.updateAssetIndex(path);
		    loger.info("updateResponse : " + updateResponse.toString());
		}

}

}