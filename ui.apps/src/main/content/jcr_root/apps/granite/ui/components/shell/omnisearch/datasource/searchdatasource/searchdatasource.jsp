<%--
  ADOBE CONFIDENTIAL
  ___________________


  Copyright 2015 Adobe
  All Rights Reserved.


  NOTICE: All information contained herein is, and remains
  the property of Adobe and its suppliers, if any. The intellectual
  and technical concepts contained herein are proprietary to Adobe
  and its suppliers and are protected by all applicable intellectual
  property laws, including trade secret and copyright laws.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe.
--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page import="java.util.Iterator,
                  java.util.Map,
				  java.util.List,
                  org.apache.sling.api.resource.ResourceWrapper,
                  org.apache.commons.collections4.Transformer,
                  org.apache.commons.collections4.iterators.TransformIterator,
                  org.apache.commons.lang3.StringUtils,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.ExpressionHelper,
                  com.adobe.granite.ui.components.ds.DataSource,
                  com.adobe.granite.ui.components.ds.EmptyDataSource,
                  com.adobe.granite.omnisearch.api.core.OmniSearchService,
                  com.day.cq.search.result.SearchResult" %>
   <%@page import="com.elasticsearch.core.service.ElasticSearchService"%><%


    final DataSource ds;
    final Map<String, String[]> predicateParameters = request.getParameterMap();
    final String location = request.getParameter("location");

    if (StringUtils.isBlank(location)) {
        // if the location is not set, return an empty collection because we are not able to mix the types
        ds = EmptyDataSource.instance();
        //out.print("if condition:  "+ location);
    } else {
        //out.print("else:  "+ location);

        final ExpressionHelper ex = cmp.getExpressionHelper();
        final Config dsCfg = new Config(resource.getChild(Config.DATASOURCE));
        final long offset = ex.get(dsCfg.get("offset", String.class), Long.class);
       	final long limit = ex.get(dsCfg.get("limit", String.class), Long.class);
        //out.print(" offset value: " +offset+ " limit value: " +limit+ " predicateParameters: " +predicateParameters);

        final ElasticSearchService elasticSearchService = sling.getService(ElasticSearchService.class);
        final boolean getESChoice = elasticSearchService.getESChoice();

        final OmniSearchService searchService = sling.getService(OmniSearchService.class);

        //final SearchResult searchResult = null;

        if(getESChoice){
            final Map<String,List<Resource>> elasticSearchResult = elasticSearchService.searchIndex(resourceResolver, predicateParameters, limit, offset);
        	
            if (elasticSearchResult == null ) {
                //out.print("mind "+ searchResult);
                ds = EmptyDataSource.instance();
            } else {

                //out.print("mindtree "+ searchResult);
                String itemResourceType = null;
                final Resource configRes = searchService.getModuleConfiguration(resourceResolver, location);


                if (configRes != null) {
                    final String itemType = dsCfg.get("itemType", "card");
                    final String itemTypePropertyName = "list".equals(itemType) ? "listItemPath" : "cardPath";
                    final ValueMap vm = configRes.getValueMap();


                    if (itemTypePropertyName.equals("cardPath")) {
                        itemResourceType = vm.get(itemTypePropertyName, "granite/ui/components/shell/omnisearch/defaultcard");
                    } else {
                        itemResourceType = vm.get(itemTypePropertyName, String.class);
                    }


                    /*if (vm.get("facets", false)) {
                        request.setAttribute("com.day.cq.search.result.SearchResult", searchResult);
                    }*/
                }


                final String itemRT = itemResourceType;
                
                ds = new DataSource() {
                    @Override
                    public Iterator<Resource> iterator() {
                        Iterator<Resource> it = elasticSearchResult.get(location).iterator();


                        if (itemRT != null) {
                            it = new TransformIterator<>(it, new Transformer<Resource, Resource>() {
                                @Override
                                public Resource transform(Resource r) {
                                    return new ResourceWrapper(r) {
                                        @Override
                                        public String getResourceType() {
                                            return itemRT;
                                        }
                                    };
                                }
                            });
                        }


                        return it;
                    }


                    @Override
                    public Long getOffset() {
                        return 0L;
                    }


                    @Override
                    public Long getLimit() {
                        return 1000L;
                    }


                    @Override
                    public Long getGuessTotal() {
                        return 10L;
                    }
                };
            
            }
		}
        else{
        	final Map<String, SearchResult> result = searchService.getSearchResults(resourceResolver, predicateParameters, limit, offset);
        	final SearchResult searchResult = result.get(location);
        	
        	if (searchResult == null ) {
                //out.print("mind "+ searchResult);
                ds = EmptyDataSource.instance();
            } else {
                //out.print("mindtree "+ searchResult);
                String itemResourceType = null;
                final Resource configRes = searchService.getModuleConfiguration(resourceResolver, location);


                if (configRes != null) {
                    final String itemType = dsCfg.get("itemType", "card");
                    final String itemTypePropertyName = "list".equals(itemType) ? "listItemPath" : "cardPath";
                    final ValueMap vm = configRes.getValueMap();


                    if (itemTypePropertyName.equals("cardPath")) {
                        itemResourceType = vm.get(itemTypePropertyName, "granite/ui/components/shell/omnisearch/defaultcard");
                    } else {
                        itemResourceType = vm.get(itemTypePropertyName, String.class);
                    }


                    if (vm.get("facets", false)) {
                        request.setAttribute("com.day.cq.search.result.SearchResult", searchResult);
                    }
                }


                final String itemRT = itemResourceType;
              
                ds = new DataSource() {
                    @Override
                    public Iterator<Resource> iterator() {
                        Iterator<Resource> it = searchResult.getResources();


                        if (itemRT != null) {
                            it = new TransformIterator<>(it, new Transformer<Resource, Resource>() {
                                @Override
                                public Resource transform(Resource r) {
                                    return new ResourceWrapper(r) {
                                        @Override
                                        public String getResourceType() {
                                            return itemRT;
                                        }
                                    };
                                }
                            });
                        }


                        return it;
                    }


                    @Override
                    public Long getOffset() {
                        return searchResult.getStartIndex();
                    }


                    @Override
                    public Long getLimit() {
                        return searchResult.getHitsPerPage();
                    }


                    @Override
                    public Long getGuessTotal() {
                        return searchResult.getTotalMatches();
                    }
                };
            }
        	
        	
        }

		//final SearchResult searchResult = tempSearchResult;

        
    }


    request.setAttribute(DataSource.class.getName(), ds);
%>
